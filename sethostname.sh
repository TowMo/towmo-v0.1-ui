# bash

if [ "$(uname)" = "Darwin" ]; then
sed -ie 's_themis.hyperdeck.net_127.0.0.1_g' ./app/filters/filters.js
sed -ie 's_= "https"_= "http"_g' ./app/filters/filters.js
sed -ie 's_https://themis.hyperdeck.net_http://127.0.0.1_g' ./www/components/loopback/lb-services.js
sed -ie 's_https://_http://_g' ./config/application.coffee
elif [ "$(expr substr $(uname -s) 1 5)" = "Linux" ]; then
sed -i 's_themis.hyperdeck.net_127.0.0.1_g' ./app/filters/filters.js
sed -i 's_= "https"_= "http"_g' ./app/filters/filters.js
sed -i 's_https://themis.hyperdeck.net_http://127.0.0.1_g' ./www/components/loopback/lb-services.js
sed -i 's_https://_http://_g' ./config/application.coffee
elif [ "$(expr substr $(uname -s) 1 10)" = "MINGW32_NT" ]; then
sed -i 's_themis.hyperdeck.net_127.0.0.1_g' ./app/filters/filters.js
sed -i 's_= "https"_= "http"_g' ./app/filters/filters.js
sed -i 's_https://themis.hyperdeck.net_http://127.0.0.1_g' ./www/components/loopback/lb-services.js
sed -i 's_https://_http://_g' ./config/application.coffee
fi
