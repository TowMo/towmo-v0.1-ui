# For an explanation of the steroids.config properties, see the guide at
# http://guides.appgyver.com/steroids/guides/project_configuration/config-application-coffee/

steroids.config.name = "Lauran Ipsum"

# -- Initial Location --
steroids.config.location = "https://localhost:3000/views/authenticate/login.html"
# steroids.config.location = "https://localhost:3000/views/create/create.html"

# -- Tab Bar --
# steroids.config.tabBar.enabled = true
# steroids.config.tabBar.tabs = [
#   {
#     title: "Requests"
#     icon: "icons/requests@2x.png"
#     location: "http://localhost/views/recipe/summary.html"
#   },
#   {
#     title: "More"
#     icon: "icons/settings@2x.png"
#     location: "updateDrawers"
#   }
# ]

# steroids.config.tabBar.tintColor = "#FFFFFF"
# steroids.config.tabBar.tabTitleColor = "#BDBDBD"
# steroids.config.tabBar.selectedTabTintColor = "#ffffff"
# steroids.config.tabBar.selectedTabBackgroundImage = "icons/pill@2x.png"

# steroids.config.tabBar.backgroundImage = ""

# -- Navigation Bar --
# steroids.config.navigationBar.tintColor = "#BDBDBD"
steroids.config.navigationBar.titleColor = "#FFFFFF"
# steroids.config.navigationBar.buttonTitleColor = "#848484"
steroids.config.navigationBar.buttonTintColor = "#FFFFFF"

# steroids.config.navigationBar.landscape.backgroundImage = ""
# steroids.config.navigationBar.portrait.backgroundImage = ""

# -- Android Loading Screen
steroids.config.loadingScreen.tintColor = "#262626"

# -- iOS Status Bar --
steroids.config.statusBar.enabled = true
steroids.config.statusBar.style = "default"

# -- File Watcher --
# steroids.config.watch.exclude = ["www/my_excluded_file.js", "www/my_excluded_dir"]

# -- Pre- and Post-Make hooks --
# steroids.config.hooks.preMake.cmd = "echo"
# steroids.config.hooks.preMake.args = ["running yeoman"]
# steroids.config.hooks.postMake.cmd = "echo"
# steroids.config.hooks.postMake.args = ["cleaning up files"]

# -- Default Editor --
# steroids.config.editor.cmd = "subl"
# steroids.config.editor.args = ["."]

# -- Drawers --
steroids.config.drawers =
  right:
  	id: "rightDrawer"
  	location: "https://localhost:3000/views/right_drawer/index.html"
  	showOnAppLoad: false
  	widthOfDrawerInPixels: 100
  	showShadow: false