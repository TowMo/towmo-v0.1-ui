// var should = require('should');
var numbers = [1, 2, 3, 4];

describe('Numbers Array:', function() {
	describe('Array Length', function() {
		it('should contain only 4 values', function() {
			numbers.length.should.equal(4);
		});
	});
	describe('All indeces should have correct value', function() {
		it('index 0 should contain 2', function() {
			numbers[1].should.equal(2);
		});
	});
});