describe('Create Directive stuff...', function() {
	var $scope
		, element
		, vehiclePicCtrl; 

	beforeEach(module('createApp'));
	beforeEach(inject(function($compile, $rootScope, $controller) {
		$rootScope.picType = {
			id: 2,
			name: 'VIN'
		};
		$scope = $rootScope;
		element = angular.element("<td vehicle-pic pic-type='picType'></td>");
		$compile(element)($scope);
		// vehiclePicCtrl = $controller('vehiclePic', {$scope: $rootScope})
	}));
	
	it("$scope should contain a 'picInfo' object", function() {
		$scope.$digest();
		// console.log(element);
	});
	
});


// <div class='icomatic' hm-release='takePic({picInfo:{type:'addPic', id:2}})' style='color:#ACACAC;font-size:50px'>camera</div><div class='icon-label'>VIN</div>