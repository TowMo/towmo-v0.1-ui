var modelStub;
var CreateDataService;
var x;

beforeEach(module('CreateModel', function($provide) {
	// $provide.value("models", {'towReqs': {id: 2}});
}));

beforeEach(inject(function(_CreateDataService_) {
	CreateDataService = _CreateDataService_;
}));

describe("Create Services Tests", function() {
	var modelTowReqPics = [
		{
			dbRecord: {
				filename: "",
				timestamp: "",
				picTypeId: 1,
				lat: "",
				long: "",
				towReqId: ""
			},
			file: {
				sourceFilename: "",
				fileType: "",
				targetFilename: "",
				fileTransfer: "",
				fileUploadOptions: {
					fileKey: "file",
					fileName: ""
				}
			}
		}, {
			dbRecord: {
				filename: "",
				timestamp: "",
				picTypeId: 2,
				lat: "",
				long: "",
				towReqId: ""
			},
			file: {
				sourceFilename: "",
				fileType: "",
				targetFilename: "",
				fileTransfer: "",
				fileUploadOptions: {
					fileKey: "file",
					fileName: ""
				}
			}
		}, {
			dbRecord: {
				filename: "",
				timestamp: "",
				picTypeId: 3,
				lat: "",
				long: "",
				towReqId: ""
			},
			file: {
				sourceFilename: "",
				fileType: "",
				targetFilename: "",
				fileTransfer: "",
				fileUploadOptions: {
					fileKey: "file",
					fileName: ""
				}
			}
		}
	];

	var models = { towReqs: {id: 20} };

	describe('Preparing TowReqPics', function() {
		it('Should return updated towReqPics', function() {
			CreateDataService.prepTowReqPics(modelTowReqPics, models);
		});
	});
});