'user strict';

beforeEach(module('AuthenticateModel'));

describe('AuthenticateModel', function() {
	describe('HostService', function() {
		var getHostInfo;
		beforeEach(inject(function(_HostService_) {
			 getHostInfo = _HostService_.getHostInfo;
		}));

		describe("getHostInfo method", function() {
			it('Should return //127.0.0.1:3000/api for the getHost Info method', function() {
				getHostInfo().should.equal('//127.0.0.1:3000/api');
			});
		});
	});

	describe('LocalStorageService', function() {
		describe('setLocalStorageService() method', function() {
			it('Should set string for a given parameter on localStorage');	
		});

		describe('getLocalStorageService() method', function() {
			it('Should return a string that was assigned to a localStorage parameter');
			it('Should return undefined for an unset parameter on LocalStorage');
		});	
	});
})