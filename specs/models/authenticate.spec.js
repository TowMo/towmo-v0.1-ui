// describe("Hello World Test", function() {
// 	var element;
// 	var $scope;

// 	beforeEach(inject(function($compile, $rootScope) {
// 		$scope = $rootScope
// 		element = angular.element("<div>{{2 + 2}}</div>");
// 		element = $compile(element)($scope);
// 	}));

// 	describe("basic shit", function() {
// 		it("HTML should show 4", function() {
// 			$scope.$digest();
// 			element.html().should.equal("4");
// 		});
// 	});
// });
beforeEach(module('AuthenticateModel'));

describe("Host Service Test", function() {
	var hostInfo;
	beforeEach(inject(function(_HostService_) {
		hostInfo = _HostService_.getHostInfo();
	}));

	describe("getHostInfo() function", function() {
		it("Should return a value of 127.0.0.1:3000/api", function() {
			hostInfo.should.equal("//127.0.0.1:3000/api");
		});	
	})
});

describe("LocalService Test", function() {
	var LocalStorageService;
	beforeEach(inject(function(_LocalStorageService_) {
		LocalStorageService = _LocalStorageService_;
	}));

	describe("setLocalStorage() function", function() {
		it("should set localStorage['user'] with 'albertwchang'", function() {
			LocalStorageService.setLocalStorage('user', 'achang28').should.ok;
		});
	});

	describe('getLocalStorage() function', function() {
		it("should return 'albertwchang' from localStorage['user']", function() {
			LocalStorageService.getLocalStorage('user').should.equal('achang28');
		})
	});

	// describe('unsetLocalStorage() function', function() {
	// 	it("should set localStorage['user'] to undefined", function() {
	// 		LocalStorageService.unsetLocalStorage('user');
	// 		LocalStorageService.getLocalStorage('user').should.be(undefined);
	// 	})
	// });
});


describe("AuthenicateService Test", function() {
	var AuthenticateService;
	var localStorageServiceSpy, hostServiceSpy;
	var hostInfo = '127.0.0.1:3000/api';

	// Mock the dependencies needed
	beforeEach(module('AuthenticateModel', function($provide) {
		localStorageServiceSpy = jasmine.createSpyObj("LocalStorageService", ["getLocalStorage", "setLocalStorage"]);
		hostServiceSpy = jasmine.createSpyObj("HostService", ["getHostInfo"]);

		hostServiceSpy.getHostInfo.and.returnValue(hostInfo);

		$provide.value('LocalStorageService', localStorageServiceSpy);
		$provide.value('HostService', hostServiceSpy);
	}));

	// What i'm going to test
	beforeEach(inject(function(_AuthenticateService_) {
		AuthenticateService = _AuthenticateService_;
		console.log(AuthenticateService);
	}));

	describe('logging in a user', function() {

		it("should send a post request to /users/login, and assign user data to local storage", function() {
			
			AuthenticateService.login2({
				username:'mfoy',
				password:'mfoy'
			}, true).should.have.properties('then');

			// LocalStorageService

		// 	user.should.have.property('username', 'achang28');
		});
	})
})