'use strict';

// describe('Filters Model Test...', function() {
// 	beforeEach(module('FiltersModel'));
// 	var getTransType;
// 	var DataService;
	
// 	beforeEach(inject(function(_DataService_){
// 		getTransType = _DataService_.getTransType;
// 		DataService = _DataService_;
// 	}));

// 	describe('getTransType()', function() {
// 		it("Should return 'Automatic' for a value of 1", function() {
// 			DataService.getTransType(1).name.should.equal("AUTOMATIC");
// 		});
// 	});
// });

describe ('Filters Test...', function() {
	var transTypeMapping;
	var dataServiceSpy;

	beforeEach(module("Filters", function($provide) {
		dataServiceSpy = jasmine.createSpyObj('DataService', ['getTransType']);
		dataServiceSpy.getTransType.and.returnValue({
			id: 1,
			name: 'MANUAL'
		});

		$provide.value("DataService", dataServiceSpy);
	}));

	beforeEach(inject(function($filter) {
		transTypeMapping = $filter("transTypeMapping");
	}));

	describe('transTypeMapping', function() {
		it("Should return 'AUTOMATIC'", function() {
			transTypeMapping(1).should.equal("AUTOMATIC");
		});
	})
})