/*
 * Default Gruntfile for AppGyver Steroids
 * http://www.appgyver.com
 *
 * Licensed under the MIT license.
 */

'use strict';

module.exports = function(grunt) {
	// grunt.initConfig({
	// 	loopback_angular: {
	//       	services: {
	//         	options: {
	//             // Task-specific options go here.
	//             input: "",
	//             output: "/components/loopback-angular/lb-services.js",
	//             apiUrl: "http://127.0.0.1:3000/api"
	//          	}
	//       	}
	//    	}
	// });

	grunt.loadNpmTasks("grunt-steroids");
	// grunt.loadNpmTasks('grunt-loopback-angular');

	grunt.registerTask("default", ["steroids-make", "steroids-compile-sass"]);
};
