angular.module('DetailsController', ['DetailsModel', 'hmTouchevents', 'Filters'])
   .controller('DetailsCtrl', function($scope, $q, DetailsDataService, SessionService, LocationService, $filter) {
      $scope.host = $filter('hostInfo')();
      $scope.isDisabled = true;
      $scope.mapAvailable = false;
      $scope.towReq;

      var settings = [{}, {backgroundColor: '#008080'}, {backgroundColor: '#FF0000'}];

      var createView = {
         view: undefined,
         location: "views/create/create.html",
         id: "createView2"
      };
      
      var backButton = new steroids.buttons.NavigationBarButton();
      var refreshButton = new steroids.buttons.NavigationBarButton();
      var rightDrawerButton = new steroids.buttons.NavigationBarButton();
      var createButton;
      
      var rightButtonSet
         , leftButtonSet;

      var towReqUpdated = false;
      
      backButton.onTap = function() {
         steroids.layers.pop();
      };

      refreshButton.onTap = function() {
         var dbQuery = "/findOne?filter=" +JSON.stringify({
                     "where": {
                        "id": $scope.towReq.id
                     },
                     "include": [
                        "vehicle", {"vehicle": "vehicleType"},
                        "towReqPics",
                        "site", {"site": "siteGeos"},
                        "statusEntries", {"statusEntries": [
                                             {"status":"party"},
                                             {"empDuty":["emp","duty"]}
                                          ]}
                     ]
                  });

         DetailsDataService.reloadModel('towReqs', dbQuery)
            .then(function(foundTowReq) {
               $scope.towReq = foundTowReq;
               $scope.$apply();
               return;
            }, function(errResults) {
               console.log("No update to TowReq: ", errResults);
            })
            .then(function() { // Get any next statuses that come after newly created status
               presentNextStatuses();
            });
      };

      rightDrawerButton.onTap = function() {
         steroids.drawers.show({
            edge: steroids.screen.edges.RIGHT
         });
      };

      $scope.setStatus = function(newStatusId) {
         navigator.geolocation.getCurrentPosition(function(foundPosition) {            
            // Get empDutyId **** for tow Users, need to compare TowReq siteId against site Id of targetSite.
            // Then, get towSiteId from found targetSite, and use it to find specific empDuty that has the same
            // siteId as found towSiteId
            console.log("Found Current Position: ", foundPosition);
            var session = SessionService.getSessionData();
            var towReqSite = DetailsDataService.getModels('towReqs').site;
            var currentEmpDuty = DetailsDataService.getEmpDuty(session.emp.empDuties, towReqSite, $scope.chosenPartyId);
            var newStatusEntry = {
               statusId: newStatusId,
               towReqId: $scope.towReq.id,
               timestamp: LocationService.getCurrentDatetime(),
               empDutyId: currentEmpDuty.id,
               // this is only a placeholder for retrieving reqEmpDutyId.  reqEmpDutyId should be obtained
               // from the current user's login id
               lat: foundPosition.coords.latitude,
               long: foundPosition.coords.longitude
            };

            // Create new status for TowReq
            DetailsDataService.createDbRecord('statusEntries', newStatusEntry)
               .then(function(statusEntryResult) {
                  console.log("statusEntries updated: ", statusEntryResult);
                  // *** Q: How do we accomodate multiple users transacting at the same time?
                  // *** Q: How does 1 user see changes that were made by another user?
                  // Get new status entry from DB
                  var dbQuery = "/findOne?filter=" +JSON.stringify({
                     "where": {
                        "id": $scope.towReq.id
                     },
                     "include": [
                        "vehicle", {"vehicle": "vehicleType"},
                        "towReqPics",
                        "site", {"site": "siteGeos"},
                        "statusEntries", {"statusEntries": [
                                             {"status":"party"},
                                             {"empDuty":["emp","duty"]}
                                          ]}
                     ]
                  });

                  console.log(dbQuery);

                  DetailsDataService.reloadModel('towReqs', dbQuery)
                     .then(function(foundTowReq) {
                        console.log("updated TowReq: ", $scope.towReq = foundTowReq);
                        $scope.$apply();
                        towReqUpdated = true;
                        return;
                     }, function(errResults) {
                        console.log("No update to TowReq: ", errResults);
                     })
                     .then(function() { // Get any next statuses that come after newly created status
                        presentNextStatuses();
                     });
               }, function(errResults) {
                  console.log("statusEntries not added: ", errResults);
               });
         },function(error) {
            deferred.reject(error);
         }, {enableHighAccuracy: false});
      };

      function presentNextStatuses() {
         // look at all statuses that come after current status
         var currentTowReqStatus = _.last(DetailsDataService.getModels('towReqs').statusEntries);
         $scope.nextStatuses = DetailsDataService.getNextStatuses(currentTowReqStatus.statusId);

         console.log("Next Statuses: ", $scope.nextStatuses);

         if ($scope.nextStatuses.length > 0) {
            steroids.addons.geolocation.ready.then(function() {
               $scope.isDisabled = false; // DISCLAIMER: proximity detection is not needed for Requestors
               $scope.$apply();
            });
         } else {
            $scope.$apply();
         }
      };

      function showMap() {
         if (navigator.connection.type !== Connection.NONE) {
            var towReqGeo = _.findWhere(DetailsDataService.getModels('towReqs').site.siteGeos, {geoTypeId: 1});
            LocationService.renderMap("map_individual", towReqGeo);
            //  1. Zooming should be dynamic -- based upon the radius of the furthest tow sign.
            // *  2. Setup a timer so that when Internet connection is NOT available, the app will "watch"
            // *     for an Internet connection every e.g. 15 seconds.
            // *  3. 
            $scope.mapAvailable = true;
            $scope.$apply();
         }
      };

      function setView(viewObj) {
         if ( (typeof viewObj.view) == 'object' ) {
            viewObj.view.unload({}, {
               onSuccess: function() {
                  // navigator.notification.alert(viewObj.view.id +" was successfully UNloaded");
               }, onFailure: function() {
                  // navigator.notification.alert("Error trying to unload " +viewObj.view.id);
               }
            });
         } else {
            viewObj.view = new steroids.views.WebView({
               location: viewObj.location,
               id: viewObj.id
            });
         }

         viewObj.view.preload({}, {
            onSuccess: function() {
               // navigator.notification.alert(viewObj.view.id +" was successfully Loaded");
            }, onFailure: function() {
               // navigator.notification.alert("Error trying to Load " +viewObj.view.id); 
            }
         });
      };

      function updateNavigation() {
         if(document.visibilityState === 'visible') {
            showMap();
            presentNextStatuses();

            if ($scope.chosenPartyId === 2)
               setView(createView);

            steroids.view.navigationBar.update({
               title: $filter('headerName'),
               overrideBackButton: true,
               buttons: {
                  left: leftButtonSet,
                  right: rightButtonSet
               }
            });
         }
      };

      function initialize() {
         //reset navigation buttons
         rightButtonSet = [];
         leftButtonSet = [];

         backButton.imagePath = "/icons/navigation/back2@2x.png";
         refreshButton.imagePath = "/icons/navigation/refresh@2x.png";
         rightDrawerButton.imagePath = "/icons/navigation/menu@2x.png";
         steroids.view.navigationBar.setAppearance({
            tintColor: settings[$scope.chosenPartyId].backgroundColor
         });

         if ($scope.chosenPartyId === 2) {
            createButton = new steroids.buttons.NavigationBarButton();
            createButton.imagePath = "/icons/navigation/create@2x.png";
            createButton.onTap = function() {
               window.postMessage({
                  recipient:"createView",
                  session: SessionService.getSessionData()
               });

               steroids.modal.show({
                  view: createView.view,
                  hidesNavigationBar: false
               });
            };

            rightButtonSet.push(createButton);
         }

         rightButtonSet.push(rightDrawerButton);
         leftButtonSet.push(backButton);
         leftButtonSet.push(refreshButton);
         updateNavigation();
      };

      var messageReceived = function(event) {
         // check that the message is intended for us
         if (event.data.recipient === "home:detailsView") {
            $scope.chosenPartyId = event.data.session.chosenPartyId;
            var party = _.findWhere(DetailsDataService.getModels('parties'), {id: $scope.chosenPartyId});
            $scope.isTowUser = ($scope.chosenPartyId === 1);
            $scope.partyName = party.name;
            $scope.towReq = event.data.towReq;
            $scope.$digest();

            DetailsDataService.setModels('towReqs', event.data.towReq);
            DetailsDataService.setModels('statuses', event.data.statuses);
            SessionService.setSessionData(event.data.session);

            initialize();
         }
      };

      window.addEventListener("message", messageReceived);
      document.addEventListener("visibilitychange", updateNavigation, false);
   });