angular.module('CreateController', ['CreateModel', 'hmTouchevents', 'Filters'])
   .controller('mainCtrl', function($q, $scope, CreateDataService, SessionService, EdmundsService, GeoLocService, FileService, $filter) {
      var backButton = new steroids.buttons.NavigationBarButton();
      backButton.imagePath = "/icons/navigation/back2@2x.png";
      // *** drawers don't work on modals!!!

      var currentContext;
      var picCount;

      var contextView = new steroids.Animation({
         transition: "slideFromRight",
         duration: 0.3,
         curve: "easeOut"
      });

      var contextHash = {
         'initVin':'inputVin',
         'initLicNumber':'inputLicNumber'
      };

      var pictureSource;
      var destinationType;
      var chosenPartyId;
      var settings = [{}, {backgroundColor: '#008080'}, {backgroundColor: '#FF0000'}];

      // DB objects
      EdmundsService.getVehicleTypes().then(function(vehicleTypes) {
         $scope.vehicleTypes = vehicleTypes;
      });

      EdmundsService.getTransTypes().then(function(foundTransTypes) {
         $scope.transTypes = foundTransTypes;
      });

      $scope.towReqPics = [];
      $scope.vehicle;
      $scope.towReq = {};
      $scope.statusEntry;

      $scope.colors = CreateDataService.getColorChoices();
      $scope.focusMode = false;
      $scope.host = $filter('hostInfo')();
      $scope.contexts = {
         'addPic':false,
         'initVin':false,
         'initVicNumber':false
      }
      
      // fetch Picture Types
      $scope.picTypes = CreateDataService.getPicTypes();

      backButton.onTap = function() {
         if (CreateDataService.getTowReqPics().length === 0 && !$scope.vehicle) {
            hideModal();
         } else {
            navigator.notification.prompt("Save work?", function(buttonResults) {
               if (buttonResults.buttonIndex === 1) {
                  // save current work
               } else {
                  hideModal();
               }
            }, "Warning",
            ["Cancel","Discard"]);
         }
      };

      function hideModal() {
         steroids.modal.hide();
      };

      $scope.changeFocus = function(context) {
         $scope.contexts[context.type] = !$scope.contexts[context.type];

         if (context.type === 'addPic') {
            // when pic already exists, need to show pic within camera mode first
            cameraOn(context.id);
         } else {
            document.getElementById(context.type).blur();
            $scope.focusMode = !$scope.focusMode;
            var targetEl = document.getElementById('pic_' +context.picTypeId);
            var chosenTowReqPic = CreateDataService.getTowReqPic('dbRecord', context.picTypeId);
            setNavigation();
            contextView.perform();
            
            if ($scope.focusMode) { // hide navbar + set corresponding picture
               // document.getElementById(contextHash[context.type]).focus();
               targetEl.src = chosenTowReqPic ? chosenTowReqPic['file'].localFilePath : null;
               targetEl.className = targetEl.src ? "img_full" : null;
            } else { // show navbar + clear picture
               targetEl.src = null;
            }
         }
      };

      function cameraOn(picTypeId) {
         picCount = picCount || 0;
         var deferred = $q.defer();
         var newTowReqPic = {
                           'dbRecord': null,
                           'file': null
                        };

         /************** Turn CAM on, and get picture *************/
         navigator.camera.getPicture(function(imageURI) {
            var sourceEl = document.getElementById('small_pic_' +picTypeId);
            sourceEl.className = "";
            sourceEl.innerHTML = "";

            // get GeoLocation data
            navigator.geolocation.getCurrentPosition(function(position) {
                  deferred.resolve(position);
               },function(geoError) {
                  deferred.reject(geoError);
               }, {enableHighAccuracy: false});

            window.resolveLocalFileSystemURI(imageURI, function(file) {
               var targetDirURI = "file://" +steroids.app.absoluteUserFilesPath;
               var beg = imageURI.lastIndexOf('/') +1
                  ,end = imageURI.lastIndexOf('.')
                  ,fileExt = imageURI.substr(end +1);
               
               var fileName = imageURI.slice(beg,end) +"-" +picCount +"." +fileExt;
               var fullFilePath = targetDirURI + "/" +fileName;
               picCount++;

               window.resolveLocalFileSystemURI(targetDirURI, function(directory) {
                  file.moveTo(directory, fileName, function(file2) {
                     var localFilePath = "/" +file2.name;
                     sourceEl.innerHTML = "<img src='" +localFilePath +"' class='img_button' />";
                     // FileService.fileCleanUp();
                     
                     newTowReqPic = {
                        'dbRecord': {
                           filename: "",
                           timestamp: GeoLocService.getCurrentDatetime(),
                           picTypeId: picTypeId
                        },
                        'file': {
                           sourceFilename: fullFilePath,
                           localFilePath: localFilePath,
                           fileType: fileExt
                        }
                     };
                  }, function(error) {
                     $scope.flashMessage = error;
                  });
               }, function(error) {
                  $scope.flashMessage = error;
               });
            });
         }, function(errMessage) {
            $scope.flashMessage = errMessage;
         }, {
            destinationType: Camera.DestinationType.FILE_URI,
            encodingType: Camera.EncodingType.JPEG,
            quality: 25,
            sourceType: Camera.PictureSourceType.CAMERA,
            saveToPhotoAlbum: false,
            targetWidth: 320,
            targetHeight: 240
         });

         deferred.promise.then(function(position) {
            newTowReqPic['dbRecord'].lat = position.coords.latitude;
            newTowReqPic['dbRecord'].long = position.coords.longitude;
            
            /******* replace picture for existing picture type, or add very 1st one **********/
            var arrayIndex = CreateDataService.findTowReq('dbRecord', picTypeId);
            CreateDataService.addTowReqPic(newTowReqPic, arrayIndex);
            // $scope.flashMessage = CreateDataService.getTowReqPics();
         });
         
      };

      $scope.processTowReq = function(chosenStatusId) {
         /******* NEED TO HAVE AT LEAST 1 SIGN POSTED IN ORDER TO SUBMIT REQUEST ********/
         var deferred = $q.defer();

         //0. Check submitted statusId (e.g. 1 or 2);
         //----------------------- SUBMITTED TOW REQS --------------------------------
         //1. Check $scope.vehicle for id !== null
         //    - null: validate vehicle, towReqPics, and statusEntry data
         if (doValidations()) return;
         if (!$scope.vehicle.id) { // 1a) Create vehicle in DB -- get vehicle.id back
            createVehicle($scope.vehicle) // --> createTowReq() --> modifyTowReqPics()
               .then(function(newVehicleId) {
                  $scope.towReq.vehicleId = $scope.vehicle.id = newVehicleId;
                  return $scope.towReq;
               }).then(function(modelTowReq) {
                  createTowReq(modelTowReq).then(function() {
                     deferred.resolve(CreateDataService.getTowReqPics()); // get && resolve ALL towReqPics (include ALL objects)
                  });
               });
         } else { //1b) Existing vehicle.id: get vehicle.id
            $scope.towReq.vehicleId = $scope.vehicle.id;
            createTowReq($scope.towReq)
               .then(function() { // --> modifyTowReqPics()
                  deferred.resolve(CreateDataService.getTowReqPics());
               }, function(err) {
                  navigator.notification.alert("Did NOT create TowReq!");
               });
         }
       
         deferred.promise.then(function(modelTowReqPics) {
            /***************** 5. Process picture files ****************/
            /********* 5a1) Prep each towReqPic for file upload *********/
            $scope.towReqPics = CreateDataService.prepTowReqPics(modelTowReqPics);

            /********* 5a2) upload each file to Storage Server *********/
            FileService.uploadTowReqPics($scope.towReqPics);

            return $scope.towReqPics;
         }).then(function(modelTowReqPics) {
            /************** 6. create towReqPic tuples in DB *************/
            CreateDataService.createDBTowReqPics(modelTowReqPics);
            return;
         }).then(function() {            
            //************ 7. Add statusEntry tuple to DB *********/
            var empDuties = SessionService.getSessionData().emp.empDuties;
            var chosenEmpSiteId = _.first(SessionService.getSessionData().chosenEmpSiteIds);            
            var empDuty = _.find(empDuties, {siteId: chosenEmpSiteId, dutyId: 6});

            /******* 7a) prep statusEntry object *******/
            var towReqPic = CreateDataService.getTowReqPic('dbRecord', 3);
            $scope.flashMessage = towReqPic;
            $scope.$apply();
            var modelStatusEntry = {
               statusId: chosenStatusId,
               empDutyId: empDuty.id,
               towReqId: $scope.towReq.id,
               timestamp: GeoLocService.getCurrentDatetime(),
               lat: towReqPic['dbRecord'].lat,
               long: towReqPic['dbRecord'].long
            };

            $scope.flashMessage2 = modelStatusEntry;

            /******** 7b. Create statusEntry record within DB **********/
            CreateDataService.createStatusEntry(modelStatusEntry);
         });

         //----------------------- SAVED TOW REQS --------------------------------
         //1. do all of the above but don't require all towReqPics.
         //2. Create vehicle entry if $scope.vehicle has a populated attribute

         // $fileUploader.isHTML5 || element.removeAttr('multiple');
         
         // scope.$emit('file:add', $fileUploader.isHTML5 ? this.files : this, scope.$eval(attributes.ngFileSelect));
         // $fileUploader.isHTML5;
         // hideModal();
      }

      function createVehicle(modelVehicle) {
         return CreateDataService.createVehicle(modelVehicle)
            .then(function(newVehicle) {
               //2. Assign vehicle.id to $scope.towReq object
               console.log("Vehicle CReated... ", newVehicle);
               return $scope.vehicle.id = newVehicle.id;
            },function(errResults) {
               console.log("Didn't add vehicle: ", errResults);
            });
      }

      function createTowReq(modelTowReq) {
         //3. Create towReq record -- assign siteId to $scope.towReq;
         // Q: What's best way to allow user to select site for which Tow Req is being created?
         modelTowReq.siteId = _.first(SessionService.getSessionData().chosenEmpSiteIds);

         console.log("modelTowReq After...", modelTowReq);
         
         return CreateDataService.createTowReq(modelTowReq)
            .then(function(newTowReq) {
               console.log("New TowReq Created: ", newTowReq);
               CreateDataService.setModel('towReqs', newTowReq);
               return $scope.towReq.id = newTowReq.id;
            }, function(errResult) {
               console.log("Couldn't create TowReq: ", errResult);
            });
      }

      function doValidations() {
         // Q: How to make this process asynchronous / parallel?
         return CreateDataService.findMissingVehicleParams($scope.vehicle, checkValidationResults)
            || CreateDataService.findMissingAuthorizeFlag($scope.authorizeFlag, checkValidationResults) ? true : false;
            // || CreateDataService.findMissingTowReqPics($scope.towReqPics, checkValidationResults)
            // || CreateDataService.findMissingTowReq($scope.towReq, checkValidationResults) ? true : false;
      };

      function checkValidationResults(results) {
         if (results.length > 0) {
            console.log("Fix missing params", results);
            return true;
         }
      };

      function getVehicleData (vin) {
         // Q: how to show user that vehicle data is in process of being retrieved?
         EdmundsService.getIntVehicleData(vin)
            .then(function(foundIntVehicleData) {    // RESULTS > 0
               if(foundIntVehicleData !== 'null' && foundIntVehicleData !== null) {
                  console.log("Internal Vehicle Results: ", foundIntVehicleData);
                  $scope.vehicle = foundIntVehicleData;
               } else {
                  EdmundsService.getExtVehicleData(vin)
                     .then(function(extVehicleData) {
                        $scope.vehicle = extVehicleData;
                     }, function(vehicleDataError) {
                        console.log("Error in searching Edmunds.com: ", vehicleDataError.message);
                     });
               }
            
            }, function(intVehicleError) { // ERROR IN TRYING TO GET DATA
               console.log("Couldn't Connect to towBYTE DB: ", intVehicleError);
            });
      };

      // Q: What happens when user has first inputed vehicle data, then inserted VIN#?
      // A: If VIN # doesn't produce any results, retain previously inputed data
      function checkVin(event) {
         var vinValue = event.target.value || null;
         console.log("vin length: ", vinValue.length);
         if (vinValue.length === 17) getVehicleData(vinValue);
         // else $scope.vehicle = {};
      };

      // function switchFocus(event) {
      //    document.getElementById(event.srcElement.id).blur();
      //    document.getElementById(event.srcElement.id).focus();
      // };

      function setNavigation() {
         if(document.visibilityState === 'visible') {
            steroids.view.navigationBar.show($filter('headerName'));
            steroids.view.navigationBar.update({
               overrideBackButton: true,
               buttons: {
                  left: [backButton],
                  right: []
               }
            });
         }
      };
   
      function messageReceived(event) {
         if (event.data.recipient === "createView") {
            chosenPartyId = event.data.session.chosenPartyId;
            SessionService.setSessionData(event.data.session);
            steroids.view.navigationBar.setAppearance({
               tintColor: settings[chosenPartyId].backgroundColor
            });
            setNavigation();
         }
      };

      window.addEventListener("message", messageReceived);
      document.addEventListener("visibilitychange", setNavigation, false);
      document.getElementById('inputVin').addEventListener('input', checkVin, false);
      // document.getElementById('licNumber-input').addEventListener('input', checkLicNumber, false);
   })