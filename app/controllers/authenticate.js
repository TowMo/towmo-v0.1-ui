var authenicateApp = angular.module('authenticateApp', ['AuthenticateModel', 'hmTouchevents'])
   .controller('loginCtrl', function($scope, AuthenticateService, FlashService) {
      $scope.isLoggedIn = false;
      $scope.showRouterView;
      $scope.flashMessage;
      $scope.reqUsers = ['cjimenez', 'mjimenez', 'mfoy', 'krafferty'];
      $scope.towUsers = ['bgonzalez', 'tgonzalez', 'afoley'];

      var homeView;
      var routerView = new steroids.Animation({
         transition: "flipHorizontalFromLeft",
         duration: 0.35,
         reversedDuration: 0.35,
         curve: "easeOut",
         reversedCurve: "easeOut"
      });

      $scope.prefill = function(user) {
         $scope.credentials = {
           username: user,
           password: user
         };

         $scope.remember = true;
         $scope.login();
      }

      // Helper function for opening new webviews
      $scope.login = function() {
         AuthenticateService.login($scope.credentials, $scope.remember)
           // Attempt to Login
            .then(function(foundUser) {
               console.log("Logged in as: ", foundUser);
               return ($scope.emp = foundUser);
            }, function(errResults) {
              $scope.flashMessage = FlashService.getMessage();
               console.log(errResults);
               return;
            }).then(function(user) { //use currentUserId to get 'emp' data
               if (!user) return;

               var partyIds = AuthenticateService.getPartyIds(user.empDuties);
               console.log("Party Ids: ", partyIds);

               if ( _.contains(partyIds, 1) && _.contains(partyIds, 2) ) {
                  $scope.showRouterView = true;  
                  routerView.perform();
               } else {
                  $scope.showRouterView = false;
                  $scope.openHomeView( _.first(user.empDuties).duty.partyId );
               }
            });
      };

      $scope.logout = function() {
         AuthenticateService.logout()
            .then(function(logoutResults) {
               console.log(logoutResults);
                $scope.showRouterView = false;
                routerView.perform();
            }, function(errResults) {
               console.log ("Couldn't log you out: ", errResults);
            });
      };

      // helper to transition to home view
      $scope.openHomeView = function(chosenPartyId) {
         $scope.showRouterView = false;
         $scope.credentials = {};
         steroids.layers.push(homeView);
         broadcastMessage(chosenPartyId);
      };

      function broadcastMessage (chosenPartyId) {
         var message = {
            recipient: "home:summaryView",
            session: {
               emp: $scope.emp,
               chosenPartyId: chosenPartyId
            }
         };

         console.log("Authenicate Controller - transmitting message: ", message);
         window.postMessage(message);
      };

      function initialize() {
         if ( (typeof homeView) == 'object' ) {
            homeView.unload({}, {
               onSuccess: function() {
                  // navigator.notification.alert("HomeView unloaded...");
               }, onFailure: function() {
                  // navigator.notification.alert("Error trying to unload HomeView."); 
               }
            });

         } else homeView = new steroids.views.WebView("views/summary/summary.html");

         homeView.preload();
      }

      function reloadPage() {
         if (document.visibilityState === "visible")
            initialize();
      }
      
      initialize();
      document.addEventListener("visibilitychange", reloadPage, false);
   })