var rightDrawerApp = angular.module('rightDrawerApp', ['RightDrawerModel', 'hmTouchevents'])
   .controller('IndexCtrl', function($scope, AuthenticateService) {
      $scope.links = [
         {id:"towReqs", name:"Reqs", location:"views/summary/summary.html", icon:"tileview"},
         {id:"emps", name:"Users", location:"views/users/index.html", icon:"group"},
         {id:"sites", name:"Sites", location:"views/sites/index.html", icon:"home"},
         {id:"whiteList", name:"Exempt", location:"views/whitelist/index.html", icon:"alert"},
         {id:"myInfo", name:"My Info", location:"views/users/personal.html", icon:"user"},
      ];

      $scope.settings = [{}, { tintColor: '#008080' }, { tintColor: '#FF0000' }];

      var loginView = new steroids.views.WebView("views/authenticate/login.html");
      
      // Helper function for opening new webviews
      // $scope.open = function(linkId) {
      //    console.log(linkId);
      //    var linkObj = _.findWhere($scope.links, {id:linkId});
      //    webView = new steroids.views.WebView(linkObj.location);
      //    console.log("WebView: ", webView);
      //    steroids.layers.pop();
      //    steroids.layers.push(webView);
      // };

      $scope.logout = function() {
         AuthenticateService.logout()
            .then(function() {
               //Q: how do we allow user to set default view (map or list)?
               loginView.unload();
               steroids.layers.popAll();
               steroids.drawers.hide();
               console.log("You just logged out");
            }, function(errResults) {
               console.log ("Couldn't log you out: ", errResults);
            });
      };

      function reloadPage() {
         if (document.visibilityState === 'visible') {
            if ( (typeof loginView ) == 'object') loginView.unload();
            loginView.preload();
         }
      };

      document.addEventListener("visibilitychange", reloadPage, false);
   })