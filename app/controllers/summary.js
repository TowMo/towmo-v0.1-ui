angular.module('SummaryControllers', ['SummaryModels', 'hmTouchevents', 'Filters'])
   // *** Q: What happens when user goes offline?
   // 1. Create flash service that returns a message for being offline
   // 2. Disable any actionable features, e.g. status updates, field edits, etc.
   .controller('SummaryCtrl', function($scope, TowReqDataService, SessionService, LocationService, $filter) {
      var chosenPartyId;
      var settings = [{}, { backgroundColor: '#008080' }, { backgroundColor: '#FF0000' }];
      var createView = {
         view: undefined,
         location: "views/create/create.html",
         id: "createView1"
      };

      var detailsView = {
         view: undefined,
         location: "views/details/details.html",
         id: "detailsView"
      };

      var modeView = new steroids.Animation({
         transition: "flipHorizontalFromLeft",
         duration: 0.3,
         reversedDuration: 0.3,
         curve: "easeOut",
         reversedCurve: "easeOut"
      });

      var modeButton = new steroids.buttons.NavigationBarButton();
      var refreshButton = new steroids.buttons.NavigationBarButton();
      var rightDrawerButton = new steroids.buttons.NavigationBarButton();
      var createButton;
      var rightButtonSet
         , leftButtonSet;

      $scope.flashMessage;
      $scope.listView = true;
      $scope.test = [];
      $scope.host = $filter('hostInfo')();
      $scope.notification = {
         noTowRequestsMessage: ""
      };

      modeButton.onTap = function() {
         $scope.listView = !$scope.listView;
         $scope.$apply();

         updateNavigation();
         modeView.perform(); // *** Research whether this view is kept in memory even after it flips back to initial view
         
         if ( !$scope.listView && (navigator.connection.type !== Connection.NONE) ) {
            var empSite = _.first(TowReqDataService.getModels('empSites'));            
            LocationService.renderMap( "map_total", _.findWhere(empSite.siteGeos, {geoTypeId: 1}) );
         }
      };

      refreshButton.onTap = function() {
         determineTargetSites();
      };

      rightDrawerButton.onTap = function() {
         steroids.drawers.show({
            edge: steroids.screen.edges.RIGHT
         });
      };

      // DOM function for opening new webviews
      $scope.openDetails = function(towReqId) {
         var foundStatuses = TowReqDataService.getModels("statuses");
         var data = {
            recipient: "home:detailsView",
            session: SessionService.getSessionData(),
            towReq: TowReqDataService.getModel('towReqs', towReqId),
            statuses: foundStatuses
         };

         window.postMessage(data);
         steroids.layers.push(detailsView.view);
      };

      function setView(viewObj) {
         if ( (typeof viewObj.view) == 'object' ) {
            viewObj.view.unload({}, {
               onSuccess: function() {
                  // navigator.notification.alert(viewObj.view.id +" was successfully UNloaded");
               }, onFailure: function() {
                  // navigator.notification.alert("Error trying to unload " +viewObj.view.id);
               }
            });
         } else {
            viewObj.view = new steroids.views.WebView({
               location: viewObj.location,
               id: viewObj.id
            });
         }

         viewObj.view.preload({}, {
            onSuccess: function() {
               // navigator.notification.alert(viewObj.view.id +" was successfully Loaded");
            }, onFailure: function() {
               // navigator.notification.alert("Error trying to Load " +viewObj.view.id); 
            }
         });
      };

      function updateNavigation() {
         if (document.visibilityState === "visible") {
            modeButton.imagePath = $scope.listView ? "/icons/navigation/map@2x.png" : "/icons/navigation/list@2x.png";
            
            if (chosenPartyId === 2)
               setView(createView);
            setView(detailsView);

            steroids.view.navigationBar.update({
               title: $filter('headerName'),
               overrideBackButton: true,
               buttons: {
                  left: leftButtonSet,
                  right: rightButtonSet
               }
            });

            determineTargetSites();
         }
      };

      function initialize() {
         //reset navigation buttons
         rightButtonSet = [];
         leftButtonSet = [];
         refreshButton.imagePath = "/icons/navigation/refresh@2x.png";
         rightDrawerButton.imagePath = "/icons/navigation/menu@2x.png";

         if (chosenPartyId === 2) {
            createButton = new steroids.buttons.NavigationBarButton();
            createButton.imagePath = "/icons/navigation/create@2x.png";
            
            createButton.onTap = function() {
               steroids.modal.show({
                  view: createView.view,
                  hidesNavigationBar: false
               });

               window.postMessage({
                  recipient:"createView",
                  session: SessionService.getSessionData()
               });
            };

            rightButtonSet.push(createButton);
         }

         steroids.view.navigationBar.setAppearance({
            tintColor: settings[chosenPartyId].backgroundColor
         });

         rightButtonSet.push(rightDrawerButton);
         leftButtonSet.push(modeButton);
         leftButtonSet.push(refreshButton);
         updateNavigation();
      };

      function determineTargetSites() {
         var targetSiteIds = SessionService.getSessionData().chosenEmpSiteIds;
         
         if (chosenPartyId === 2) { 
            getTowReqs(targetSiteIds); // use visibleStatuses from Model/Services 
         } else { // conversion to requestor sites served by tow company
            var dbQuery = "?filter=" +JSON.stringify({
                                       "where": {
                                          "siteId": {
                                             "inq": targetSiteIds
                                          }
                                       },
                                       "include":"siteGeos"
                                    });

            TowReqDataService.reloadModels('sites', dbQuery)
               .then(function(foundSites) {
                  return _.pluck(foundSites, 'id');
               }).then(function(foundSiteIds) {
                  getTowReqs(foundSiteIds);
               });
         }
      };

      function getTowReqs(siteIds) { // Fetch corresponding towReqs by siteId
         var dbQuery = "?filter="
            +JSON.stringify({
               'where': {
                  'siteId': {
                     'inq': siteIds
                  }
               },
               'include': [
                  'vehicle', {'vehicle':'vehicleType'},
                  'towReqPics',
                  'site', {'site':['siteGeos','site']},
                  'statusEntries', {'statusEntries':[
                        {'status':'party'}, 
                        {'empDuty':['emp','duty','site']}
                     ]
                  }
               ]
            });
         
         TowReqDataService.reloadModels('towReqs', dbQuery)
            .then(function(foundTowReqs) { // 1. find towReqs based on applicable SiteIds
               return $scope.towReqs = foundTowReqs;
               console.log("TowReqs: ", foundTowReqs);
            }).then(function(foundTowReqs) { // 2. filter out towReqs based on allowed statuses
               if (chosenPartyId === 1) {
                  TowReqDataService.filterTowReqs(foundTowReqs);
                  $scope.towReqs = TowReqDataService.getModels('towReqs');
               }
               
               return;
            }).then(function() { // 3. set page notifications for 0 results
               // if ($scope.towReqs.length === 0 || !$scope.towReqs) {
               //    // $scope.notification.noTowRequests = NotificationService.getNotification('noTowRequests');s
               //    $scope.notification.noTowRequestsMessage = "You have no Tow Requests";
               // } else {
               //    $scope.notification.noTowRequestsMessage = false;
               // }
            });
      };

      function messageReceived(event) {
         if (event.data.recipient === "home:summaryView") {
            chosenPartyId = event.data.session.chosenPartyId;
            $scope.isTowUser = (chosenPartyId === 1);
            event.data.session.chosenEmpSiteIds = [];
            var allSites = _.pluck(event.data.session.emp.empDuties, 'site');
            
            var filteredSites = _.filter(allSites, function(site) {
                                 return site.partyId === chosenPartyId;
                              });

            var empSites = [];
            empSites.push(_.first(filteredSites));
            TowReqDataService.setModels('empSites', empSites);
            TowReqDataService.setModels('sites', empSites);
   
            // ************** temporary: getting only the 1st site Id
            event.data.session.chosenEmpSiteIds.push(_.first(empSites).id);
            SessionService.setSessionData(event.data.session);

            var towStatusQuery = JSON.stringify({
                                    "where": {
                                       "prevStatusId": {
                                          "neq":null
                                       }
                                    }
                                 });

            var reqStatusQuery = JSON.stringify({
                                    "where": {
                                       "prevStatusId": {
                                          "lt":3
                                       }
                                    }
                                 });

            var dbQuery = (chosenPartyId === 1) ? "?filter" +towStatusQuery : "?filter" +reqStatusQuery;
            TowReqDataService.reloadModels('statuses', dbQuery);

            // page initialization stuff
            var party = _.findWhere(TowReqDataService.getModels('parties'), {id: chosenPartyId});
            $scope.partyName = party.name;
            $scope.user = SessionService.getSessionData().emp;
            $scope.$apply();

            initialize();
         }
      };

      window.addEventListener("message", messageReceived);
      document.addEventListener("visibilitychange", updateNavigation, false);
      // Add event listener for connection going down
      // Add event listener for connection returning
   })