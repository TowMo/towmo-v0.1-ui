angular.module('Filters', ['FiltersModel'])
	.filter('transTypeMapping', function(DataService) {
		return function(transTypeId) {
			return DataService.getTransType(transTypeId).name;
		}
	})
	.filter('statusMapping', function(DataService) {
		return function(statusId) {
			return DataService.getStatus(statusId).name;
		}
	})
	.filter('hostInfo', function() {
		return function() {
			var protocol = "https",
				hostname = "://themis.hyperdeck.net",
				port = ':3000',
				directory = "/api";

			return (protocol +hostname +port +directory);
		};
	})
	.filter('headerName', function() {
		return 'TowMo';
	})
