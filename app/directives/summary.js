var summaryApp = angular.module('summaryApp', ['SummaryControllers', 'Filters'])
	.directive('partyPic', function() {
		/* Requestor View:
		1) show picture of Requestor User when most recent status performed by requestor user
		2) show picture of Tow Site when most recent status performed by tow user 
		*/

		/* Tow User View:
		1) show picture of Requestor Site when most recent status performed by requestor user
		2) show picture of Tow Driver when most recent status performed by tow user
		*/ 
		return {
			restrict: 'E',
			scope: {
				host: '@',
				partyName: '@',
				towReq: '=model'
			},
			template: "<img class='img_summary' ng-src='{{picFilePath}}' /><div>{{picName}}</div>",
			link: function(scope, element, attrs) {
				var resource = "emps";
				var filename;
				var empDuty = scope.towReq.statusEntries[scope.towReq.statusEntries.length - 1].empDuty;
				scope.picName = empDuty.emp.firstName +" " +empDuty.emp.lastName.substr(0,1) +".";

				scope.$watch('partyName', function(partyName) {
					if (partyName === "Tower") {
						if (empDuty.duty.partyId === 1) {
							filename = empDuty.emp.filename;
						} else {
							filename = "nobody.jpg";
							scope.picName = "(none)";
						}
					} else { // current user is a Requestor
						if (empDuty.duty.partyId === 1) {
							filename = scope.towReq.site.site.filename;
							scope.picName = null;
							resource = "sites"; // show company logo of tow site
						} else {
							filename = empDuty.emp.filename; // show picture of requestor employee
						}
					}

					scope.picFilePath = scope.host +"/containers/" +resource +"/download/" +filename;
				});
			}
		}
	})