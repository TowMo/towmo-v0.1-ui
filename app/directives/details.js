var detailsApp = angular.module('detailsApp', ['DetailsController', 'Filters'])
	// .directive('statusButtons', function() {
	// 	return {
	// 		restrict: 'E',
	// 		scope: {
	// 			nextStatuses: "=model",
	// 			setStatus: "&action"
	// 		},
	// 		template:
	// 			"<table>"
	// 				+"<tr>"
	// 					+"<td ng-repeat='nextStatus in nextStatuses'>"
	// 						+"<button id='status_button' class='topcoat-button--cta' ng-disabled='isDisabled' hm-release='setStatus({statusId: nextStatus.id})'>{{ nextStatus.buttonName }}</button>"
	// 					+"</td>"
	// 				+"</tr>"
	// 			+"</table>"
	// 	}
	// })
	.directive('siteInfo', function() {
		return {
			restrict: 'E',
			scope: {
				host: '@',
				partyId: '@',
				towReq: '=model'
			},
			template: "<img ng-src='{{host}}/containers/sites/download/{{site.filename}}' /><div>{{site.name}}<br />{{site.street}}<br />{{site.city}}, {{site.stateId}} {{site.zip}}<br />{{site.phone}}</div>",
			link: function(scope) {
				scope.$watch('partyId', function(partyId) {
					if (partyId == 1) scope.site = scope.towReq.site;
					else if (partyId == 2) scope.site = scope.towReq.site.site;
				});
			}
		}
	})
	.directive('empInfo', function(SessionService) {
		return {
			restrict: 'E',
			scope: {
				host: '@',
				partyId: '@',
				towReq: '=model'
			},
			template:
				"<img ng-src='{{host}}/containers/emps/download/{{emp.filename}}' />"
				+"<div>{{emp.firstName}} {{emp.lastName.substr(0,1)}}.</div>"
				+"<div>{{emp.phone}}</div>",
			link: function(scope) {
				var recentEmpDuty	
				var firstEmpDuty;

				scope.$watch('towReq', function(currentTowReq) {
					console.log("currentTowReq: ", currentTowReq);
					latestEmpDuty = currentTowReq.statusEntries[currentTowReq.statusEntries.length - 1].empDuty;
					recentEmpDuty = _.last(currentTowReq.statusEntries).empDuty;
					
					if (scope.partyId === 1) {	
						if (latestEmpDuty.duty.partyId === 1)
							scope.emp = recentEmpDuty.emp;
						else
							scope.emp = {
								filename: "nobody.jpg",
								firstName: "(none)",
								lastName: null,
								phone: null
							};
					} else
						scope.emp = recentEmpDuty.emp; // show picture of requestor employee
				});
			}
		}
	})