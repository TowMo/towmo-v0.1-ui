angular.module('AuthenticateModel', ['ngSanitize', 'lbServices', 'Filters'])
	.factory('AuthenticateService', function($http, $q, LocalStorageService, FlashService, User, $filter) {
		// **** need to figure out a way to inject Angular objects -- doesn't work here
		var host = $filter('hostInfo')();

		function sanitizeCredentials(credentials) {
			return {
				username: credentials.username,
				password: credentials.password
			};
		};

		function getEmpDuties(empId) {
			var deferred = $q.defer();
			var dbQuery = "/emps/findOne?filter=" +JSON.stringify({
				"where": {
					"id": empId
				},
				"include": {
					"empDuties": ["duty",{"site":"siteGeos"}]
				}
			});

			$http.get(host +dbQuery)
					.then(function(foundEmp) {
						console.log(foundEmp);
						if (foundEmp.data) deferred.resolve(foundEmp.data);
						else deferred.reject("No record found");
					}, function(errResults) {
						deferred.reject(errResults.data);
					});

			return deferred.promise;
		};

		return {
			login: function(credentials, rmbrChoice) {
				var deferred = $q.defer();


				User.login({include: 'User', remember: rmbrChoice}, sanitizeCredentials(credentials)).$promise
					.then(function(foundUser) {
						getEmpDuties(foundUser.userId)
							.then(function(foundEmp) {
								// extract party Ids from ALL records								
								deferred.resolve(foundEmp);
							}, function(errResults) {
								console.log("Couldn't get Emp Data: ", errResults.data);
							});
					}, function(errResults) {
						FlashService.setMessage("*** Bad Login Credentials!");
						console.log("Set Flash Message w/ Error: ", FlashService.getMessage());
						deferred.reject(errResults.data);
					});

				// $http.post(host +"/users/login?include=user&remember="+rmbrChoice, sanitizeCredentials(credentials))
				// 	.then(function(foundUser) {
				// 		getEmpDuties(foundUser.data.userId)
				// 			.then(function(foundEmpDuties) {
				// 				// extract party Ids from ALL records
				// 				foundUser.data.empDuties = foundEmpDuties;
				// 				console.log("Found User Data: ", foundUser.data);
								
				// 				deferred.resolve(foundUser.data);
				// 			}, function(errResults) {
				// 				console.log("Couldn't get Emp Data: ", errResults.data);
				// 			});

    //   					LocalStorageService.setLocalStorage("$LoopBack$accessTokenId", foundUser.data.id);
    //   					LocalStorageService.setLocalStorage("$LoopBack$currentUserId", foundUser.data.userId);
				// 	}, function(errResults) {
				// 		FlashService.setMessage("*** Bad Login Credentials!");
				// 		console.log("Set Flash Message w/ Error: ", FlashService.getMessage());
				// 		deferred.reject(errResults.data);
				// 	});

				return deferred.promise;
			},

			logout: function(FlashService) {
				var deferred = $q.defer();

				$http.post(host +"/Users/logout?access_token=" +LocalStorageService.getLocalStorage("$LoopBack$accessTokenId"))
					.then(function(logoutResults) {
      					LocalStorageService.unsetLocalStorage("$LoopBack$accessTokenId");
      					LocalStorageService.unsetLocalStorage("$LoopBack$currentUserId");
      					deferred.resolve(logoutResults.data);
					}, function(errResults) {
						deferred.reject(errResults.data);
					});

				return deferred.promise;
			},

			getPartyIds: function(empDuties) {
				return _.map(empDuties, function(empDuty) {
							return empDuty.duty.partyId;
						});
			}
		};
	})
	.factory('LocalStorageService', function() {
		return {
			setLocalStorage: function(param, value) {
				localStorage[param] = value;
				return true;
			},
			getLocalStorage: function(param) {
				return localStorage[param];
			},
			unsetLocalStorage: function(param) {
				localStorage.removeItem(param);
			}
		};
	})
	.factory('FlashService', function() {
		var flashMessage = "";
		
		return {
			setMessage: function(message) {
				flashMessage = message;
			},

			getMessage: function() {
				return flashMessage;
			}
		};
	})