// The contents of individual model .js files will be concatenated into dist/models.js
angular.module('DetailsModel', ['Filters'])
	.factory('DetailsDataService', function($http, $q, SessionService, $filter) {
		var host = $filter('hostInfo')();
		var models = ['towReqs', 'statuses', 'sites', 'empSites', 'parties', 'test'];
		
		var permissionsMap = [
			{partyId: 1, dutyId: 1},
			{partyId: 2, dutyId: 6}
		];

		models['parties'] = [
			{id: 1, name: 'Tower'},
			{id: 2, name: 'Requestor'}
		];

		function fetchModels(resource, query) {
			var deferred = $q.defer();

			$http.get(host +"/" +resource +query)
				.success(function(foundModels) {
					deferred.resolve(models[resource] = foundModels);
				}).error(function(errResults) {
					console.log("***Couldn't get " +resource +": ", errResults);
					deferred.reject(errResults);
				});

			return deferred.promise;
		};

		return {
			getModels: function(resource) {
				return models[resource];
			},

			setModels: function(resource, data) {
				models[resource] = data;
			},

			reloadModel: function(resource, dbQuery) {
				var deferred = $q.defer();
				$http.get(host +"/" +resource +dbQuery)
					.success(function(foundModel) {
						models[resource] = foundModel
						deferred.resolve(foundModel);
					}).error(function(errResults) {
						deferred.reject(errResults);
					});

				return deferred.promise;	
			},

			getEmpDuty: function(empDuties, towReqSite, chosenPartyId) {
				var permission = _.findWhere(permissionsMap, {partyId: chosenPartyId});
				
				/******* Requestor User: compare towReqSite.id vs. Requestor's empDuties[].siteId ********/
				/******* Tow User: compare towReqSite.towSiteId vs. empDuties[].siteId ********/
				// var siteId = (chosenPartyId === 1) ? towReqSite.towSiteId : towReqSite.id;
				var siteId = (chosenPartyId === 1) ? towReqSite.siteId : towReqSite.id;
				return _.find(empDuties, function(empDuty) {
						if ( (empDuty.siteId === siteId) && (empDuty.dutyId === permission.dutyId) && (empDuty.duty.partyId === chosenPartyId) )
							return empDuty;	
					});
 			},

			getStatus: function(statusId) {
				return _.findWhere(models['statuses'], {id: statusId});
			},

			getNextStatuses: function(currentTowReqStatusId) {
				console.log("Current TowReq Status Id: ", currentTowReqStatusId);
				var chosenPartyId = SessionService.getSessionData().chosenPartyId;
				return _.where(models['statuses'], {prevStatusId: currentTowReqStatusId, partyId: chosenPartyId});
			},

			createDbRecord: function(resource, data) {
				var deferred = $q.defer();

				$http.post(host +"/" +resource, data)
					.success(function(results) {
						deferred.resolve(results);
					}).error(function(errResults) {
						deferred.reject(errResults);
					});

				return deferred.promise;
			}
		};
	})
	.factory('SessionService', function() {
		var sessionData;

		return {
			setSessionData: function(currentSessionData) {
				sessionData = currentSessionData;
			},
			getSessionData: function() {
				return sessionData;
			}
		}
	})
	.factory('LocationService', function(DetailsDataService, $q, $http) {
		var geoMarkers = [];
		var map;
		var models = ['statusEntry'];

		function createMapMarker(geoPosition, icon) {
			var marker = new google.maps.Marker({
				position: new google.maps.LatLng(geoPosition.lat, geoPosition.long),
				icon: icon,
				map: map,
				animation: google.maps.Animation.DROP
			});

			geoMarkers.push(marker);
		};

		function buildMap(towReqGeo, callback, divId, zoomLevel) {
			//DECLARE MAP BEHAVIOR
			var mapOptions = {
				center: new google.maps.LatLng(towReqGeo.lat, towReqGeo.long),
				zoom: zoomLevel,
				mapTypeId: google.maps.MapTypeId.ROADMAP
			};

			//INSTATIATE MAP OBJECT
			map = new google.maps.Map(document.getElementById(divId), mapOptions);
			var icon = {
				path: google.maps.SymbolPath.CIRCLE,
				scale: 6,
				strokeColor: '#0101DF'
			};

			createMapMarker(towReqGeo, icon); //create Map Marker for center point
			models['statusEntry'] = DetailsDataService.getModels('towReqs');
			var statusEntry = _.first(DetailsDataService.getModels('towReqs').statusEntries);
			callback(statusEntry, null); // calls 'createMapMarker'
		};

		return {
			getCurrentDatetime: function() {
				return moment().format('YYYY-MM-DD h:mm:ss');
			},

			getModels: function(resource) {
				return models[resource];
			},

			renderMap: function(divId, towReqGeo) {
				// var towReqGeo = {latitude: towReqGeo.lat, longitude: towReqGeo.long};
				buildMap(towReqGeo, createMapMarker, divId, 19);
			}
		}
	})