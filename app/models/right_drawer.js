angular.module('RightDrawerModel', ['Filters'])
	.factory('AuthenticateService', function($http, $q, LocalStorageService, $filter) {
		// **** need to figure out a way to inject Angular objects -- doesn't work here
		var host = $filter('hostInfo')();

		function sanitizeCredentials(credentials) {
			return {
				username: credentials.username,
				password: credentials.password
			};
		};

		return {
			logout: function() {
				var deferred = $q.defer();

				$http.post(host +"/Users/logout?access_token=" +LocalStorageService.getLocalStorage("$LoopBack$accessTokenId"))
					.then(function(logoutResults) {
      					LocalStorageService.unsetLocalStorage("$LoopBack$accessTokenId");
      					LocalStorageService.unsetLocalStorage("$LoopBack$currentUserId");
      					deferred.resolve(logoutResults.data);
					}, function(errResults) {
						deferred.reject(errResults.data);
					});

				return deferred.promise;
			}
		};
	})
	.factory('LocalStorageService', function() {
		return {
			setLocalStorage: function(param, value) {
				localStorage[param] = value;
			},
			getLocalStorage: function(param) {
				return localStorage[param];
			},
			unsetLocalStorage: function(param) {
				localStorage.removeItem(param);
			}
		};
	})