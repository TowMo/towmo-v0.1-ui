angular.module('FiltersModel', [])
	.factory('DataService', function() {
		var transTypes = [
			{id:1, name:'AUTOMATIC'},
			{id:2, name:'MANUAL'}
		];
		var statuses = [
			{id:1, name:'Saved'},
			{id:2, name:'Requested'},
			{id:3, name:'Cancelled'},
			{id:4, name:'Timed Out'},
			{id:5, name:'No Vehicle'},
			{id:6, name:'Towed'},
			{id:7, name:'Released'},
			{id:8, name:'Impounded'}
		];

		return {
			getTransType: function(transTypeId) {
				return _.findWhere(transTypes, {id: transTypeId});
			},
			getStatus: function(statusId) {
				return _.findWhere(statuses, {id: statusId});
			}
		};
	})