// The contents of individual model .js files will be concatenated into dist/models.js
angular.module('SummaryModels', ['Filters'])
	.factory('TowReqDataService', function($http, $q, SessionService, $filter) {
		var host = $filter('hostInfo')();
		var models = ['towReqs', 'statuses', 'sites', 'empSites', 'parties', 'test'];
		var permissionsMap = [
			{partyId: 1, dutyId: 1},
			{partyId: 2, dutyId: 6}
		];

		models['parties'] = [
			{id: 1, name: 'Tower'},
			{id: 2, name: 'Requestor'}
		];

		function fetchModels(resource, query) {
			var deferred = $q.defer();

			$http.get(host +"/" +resource +query)
				.success(function(foundModels) {
					deferred.resolve(models[resource] = foundModels);
				}).error(function(errResults) {
					console.log("***Couldn't get " +resource +": ", errResults);
					deferred.reject(errResults);
				});

			return deferred.promise;
		};

		return {
			getModels: function(resource) {
				return models[resource];
			},

			reloadModels: function(resource, dbQuery) {
				console.log("Reloading " +resource +"...");
				dbQuery = dbQuery || "";

				return fetchModels(resource, dbQuery)
					.then(function(foundModels) {
						return models[resource] = foundModels;
					}, function(errResults) {
						console.log("Couldn't get " +resource +": ", errResults);
					});
			},

			getModel: function(resource, resourceId) {
				return _.findWhere(models[resource], {id: resourceId});
			},

			setModels: function(resource, data) {
				models[resource] = data;
			},

			filterTowReqs: function(towReqs) {
				console.log("Filtering TowReqs...");
				models['towReqs'] = _.filter(towReqs, function(towReq) {
										var result = _.find(models['statuses'], function(status) {
											return status.id === _.last(towReq.statusEntries).statusId;
										});

										// console.log("found Status: ", result);
										if (result) return towReq;
									});
			}
		};
	})
	.factory('SessionService', function() {
		var sessionData;

		return {
			setSessionData: function(currentSessionData) {
				sessionData = currentSessionData;
			},
			getSessionData: function() {
				return sessionData;
			}
		}
	})
	.factory('LocationService', function(TowReqDataService) {
		var geoMarkers = [];
		var map;

		function createMapMarkers() {
			_.each(TowReqDataService.getModels('towReqs'), function(towReq) {
				var applicableStatus = _.first(towReq.statusEntries);
				var statusPosition = {latitude: applicableStatus.lat, longitude: applicableStatus.long};
				createMapMarker(statusPosition, null);
			})
		};

		function createMapMarker(position, icon) {
			var marker = new google.maps.Marker({
				position: new google.maps.LatLng(position.latitude, position.longitude),
				icon: icon,
				map: map,
				animation: google.maps.Animation.DROP
			});

			geoMarkers.push(marker);
		};

		function buildMap(empSiteGeo, callback, divId, zoomLevel) {
			//DECLARE MAP BEHAVIOR
			var mapOptions = {
				center: new google.maps.LatLng(empSiteGeo.latitude, empSiteGeo.longitude),
				zoom: zoomLevel,
				mapTypeId: google.maps.MapTypeId.ROADMAP
			};

			//INSTATIATE MAP OBJECT
			map = new google.maps.Map(document.getElementById(divId), mapOptions);
			var icon = {
				path: google.maps.SymbolPath.CIRCLE,
				scale: 6,
				strokeColor: '#0101DF'
			}

			createMapMarker(empSiteGeo, icon); //create Map Marker for center point
			callback(); // calls 'createMapMarkers'
		};

		return {
			renderMap: function(divId, siteGeo) {
				var empSiteGeo = {latitude:siteGeo.lat, longitude:siteGeo.long};
				buildMap(empSiteGeo, createMapMarkers, divId, 17);
			}
		}
	})
	// .factory('NotificationService', function() {
	// 	var notifications = {
	// 		connectionDown: "Lost data connection",
	// 		connectionUp: "Data connection back",
	// 		noTowRequests: "Currently no requests"
	// 	};

	// 	return {
	// 		getNotification: function(notificationType) {
	// 			return notifications[notificationType);
	// 			// return "Currently No REquests";
	// 		}
	// 	};
	// })