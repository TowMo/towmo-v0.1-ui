// The contents of individual model .js files will be concatenated into dist/models.js
angular.module('CreateModel', ['Filters'])
	.factory('CreateDataService', function($http, $q, $filter) {
		var host = $filter('hostInfo')();
		var models = ['towReqs'];
		var requiredVehicleParams = ['vin', 'licNumber', 'make', 'model', 'color', 'vehicleTypeId', 'year', 'transTypeId'];
		var picTypes;
		var towReqPics = [];

		return {
			setModel: function(param, data) {
            models[param] = data;
         },

			getColorChoices: function() {
				return [
					{name:"red", code:"#FF0000"},
					{name:"blue", code:"#0040FF"},
					{name:"green", code:"#088A29"},
					{name:"white", code:"#FFFFFF"},
					{name:"silver", code:"#BDBDBD"},
					{name:"black", code:"#000000"},
					{name:"brown", code:"#B45F04"},
					{name:"grey", code:"#A4A4A4"},
					{name:"purple", code:"#7401DF"},
					{name:"yellow", code:"#FFFF00"},
					{name:"orange", code:"#FF8000"},
					{name:"pink", code:"#F6CEEC"}
				];
			},
			
			getSamplePic: function(picTypeId) {
				return _.find(samplePics, {picTypeId: picTypeId});
			},

			getPicTypes: function() {
				return [
					{id:1, name:"VIN"},
					{id:2, name:"Lic No"},
					{id:3, name:"Vehicle"}
				];
			},

			addTowReqPic: function(towReqPic, arrayIndex) {
				var msg = "adding towReqPic into array...";
				// navigator.notification.alert(msg);
				
				if (arrayIndex) towReqPics[arrayIndex] = towReqPic;
				else towReqPics.push(towReqPic);
			},

			getTowReqPics: function(param) {
				return param ? _.pluck(towReqPics, param) : towReqPics;
			},

			getTowReqPic: function(param, picTypeId) {
				var foo = _.find(towReqPics, function(towReqPic) {
					if (towReqPic[param].picTypeId == picTypeId)
						return towReqPic;
				});

				return foo;
			},

			findTowReq: function(param, picTypeId) {				
				var index = _.findKey(towReqPics, function(towReqPic) {
							if (towReqPic[param].picTypeId == picTypeId)
								return true;
						});
				
				return index;
			},

			setTowReqPics: function(updatedTowReqPics) {
				towReqPics = updatedTowReqPics;
			},

			prepTowReqPics: function(modelTowReqPics) {
				console.log("in prepTowReqPics: ", modelTowReqPics);
				console.log("towReq: ", models['towReqs']);
				towReqPics = _.map(modelTowReqPics, function(towReqPic) {
							// 1. prepare object to have server filename of the picture
							var filename = models['towReqs'].id +"-" +towReqPic['dbRecord'].picTypeId +"." +towReqPic['file'].fileType
			            towReqPic['dbRecord'].towReqId = models['towReqs'].id;
			            towReqPic['dbRecord'].filename = towReqPic['file'].targetFilename = filename;

			            //build File Transfer object + parameters
			            towReqPic['file'].fileTransfer = new FileTransfer();
			            towReqPic['file'].fuOptions = new FileUploadOptions();
			            towReqPic['file'].fuOptions.fileKey = "file";
						   towReqPic['file'].fuOptions.fileName = filename;
						            							// mimeType: "image/"+towReqPic.fileType // does 'jpg' have to be shown as 'jpeg'???
						            
			            return towReqPic;
         			});
				console.log("modified TowReqPics: ", towReqPics);
				return towReqPics;
			},

			findMissingAuthorizeFlag: function(authorizeFlag, checkValidationResults) {
				return !authorizeFlag;
			},

			findMissingVehicleParams: function(vehicle, checkValidationResults) {
				var findings = _.filter(requiredVehicleParams, function(param) {
					return !vehicle.hasOwnProperty(param);
				});

				return checkValidationResults(findings);
			},

			createVehicle: function(vehicleInfo) {
				return this.createDBRecord(vehicleInfo, "/vehicles");
			},

			createTowReq: function(towReqInfo) {
				return this.createDBRecord(towReqInfo, "/towReqs");
			},

			createDBTowReqPics: function(towReqPicsArray) {
				for (cnt = 0; cnt < towReqPicsArray.length; cnt++) {
					this.createDBRecord(towReqPicsArray[cnt].dbRecord, "/towReqPics")
						.then(function(newTowReqPic) {
							console.log("TowReqPic created: ", newTowReqPic);
						}, function(err) {
							console.log("Can't create TowReqPic: ", err.error);
						});
				}
			},

			createStatusEntry: function(modelStatusEntry) {
				return this.createDBRecord(modelStatusEntry, "/statusEntries");
			},

			createDBRecord: function(data, resource) {
				var deferred = $q.defer();

				$http.post(host +resource, data)
					.success(function(createdRecord) {
						console.log()
						deferred.resolve(models[resource] = createdRecord);
					}).error(function(errStatus) {
						deferred.reject(errStatus);
					});

				return deferred.promise;
			}
		}
	})
	.factory('FileService', function($filter) {
		var host = $filter('hostInfo')();
		var resource = '/containers/vehicles/upload';

		return {
			uploadTowReqPics: function(towReqPics) {
				_.each(towReqPics, function(towReqPic) {
					towReqPic['file'].fileTransfer.upload(
						towReqPic['file'].sourceFilename,
						encodeURI(host +resource),
						function(res) {
							// navigator.notification.alert("Success: " +res.responseCode +res.response +res.bytesSent);
						}, function(err) {
							// navigator.notification.alert("Error: " +err.code +err.source +err.target);
						}, towReqPic['file'].fuOptions);

            	// ft.abort();
				});
			},

			fileCleanUp: function() {
				navigator.camera.cleanup(function onSuccess() {
				    navigator.notification.alert("file cleanup Successful");
				}, function onFail(message) {
				    navigator.notification.alert("Cleanup Failed: ", message);
				});
			}
		}
	})
	.factory('FlashService', function() {
		var message;

		return {
			setMessage: function(incomingMessage) {
				message = incomingMessage;
			},

			getMessage: function() {
				return message;
			}
		}
	})
	.factory('EdmundsService', function($http, $q, $filter) {
		var host = $filter('hostInfo')();
		var vehicleTypes;
		var transTypes;
		var apiKey = 'dq3n4f4xrm6yky44yu23vn6p';

		var fetchVehicleTypes = function() {
			var deferred = $q.defer();

			$http.get(host +"/vehicleTypes")
				.success(function(results) {
					deferred.resolve(vehicleTypes = results)
				})
				.error(function(errResults) {
					deferred.reject(errResults.data);
				})

			return deferred.promise;
		};

		var fetchTransTypes = function() {
			var deferred = $q.defer();

			$http.get(host +"/transTypes")
				.success(function(results) {
					deferred.resolve(transTypes = results)
				})
				.error(function(errResults) {
					deferred.reject(errResults.data);
				})

			return deferred.promise;
		};

		return {
			getIntVehicleData: function (vin) {
				var deferred = $q.defer();
				var dbQuery = "/vehicles/findOne?filter=" +JSON.stringify({
										"where": {
											"vin":vin
										},
										"include":["vehicleType","transType"]
									});

				console.log("checking internal DB from host: ", host +dbQuery);

				$http.get(host +dbQuery)
					.then(function(foundVehicle) {
						deferred.resolve(foundVehicle);
					},function(errResults) {
						if (errResults.status == 404) deferred.resolve(null);
						else deferred.reject(errResults.data);
					});

				return deferred.promise;
			},

			getExtVehicleData: function (vin) {
				var deferred = $q.defer();
				
				// returns an object (instead of a $promise)
				$http.get("https://api.edmunds.com/api/vehicle/v2/vins/" +vin
					+"?fmt=json&api_key=" +apiKey)
					.success(function(foundVehicle) {
						var extVehicleInfo;

						console.log("checking Edmunds...");
						if (foundVehicle.hasOwnProperty('warning')) {
							extVehicleInfo = {vin: vin};
						} else {
							console.log("foundVehicle: ", foundVehicle);
							extVehicleInfo = {
								make: foundVehicle.make.name,
								model: foundVehicle.model.name,
								// color: null,
								year: _.first(foundVehicle.years).year,
								vin: foundVehicle.vin,
								// licNumber: null,
								transTypeId: _.find(transTypes, {name: foundVehicle.transmission.transmissionType}).id,
								vehicleTypeId: _.find(vehicleTypes, {name: foundVehicle.categories.vehicleType}).id
							};

							console.log("extVehicleInfo: ", extVehicleInfo);
						}

						deferred.resolve(extVehicleInfo);
					})
					.error(function(errResults) {
						deferred.reject(errResults);
					});

				return deferred.promise;
			},

			getVehicleTypes: function() {
				var deferred = $q.defer();

				if (!vehicleTypes) {
					fetchVehicleTypes().then(function(results) {
						deferred.resolve(vehicleTypes = results);
					});
				} else {
					deferred.resolve(vehicleTypes);
				}

				return deferred.promise;
			},

			getVehicleType: function(vehicleTypeId) {
				return _.find(vehicleTypes, {id: vehicleTypeId});	
			},

			getTransTypes: function() {
				var deferred = $q.defer();

				if (!transTypes) {
					fetchTransTypes().then(function(results) {
						deferred.resolve(transTypes = results);
					});
				} else {
					deferred.resolve(transTypes);
				}

				return deferred.promise;
			},

			getTransType: function(transTypeId) {
				return _.find(transTypes, {id: transTypeId});
			},

			setVehicleTypes: function(vehicleTypesArray) {
				vehicleTypes = vehicleTypesArray;
			}
		}
	})
	.factory('GeoLocService', function($q) {
		return {
			getGeoPosition: function() {
				var deferred = $q.defer();

				navigator.geolocation.getCurrentPosition(function(position) {
					deferred.resolve(position);
					console.log("Obtained Current location...");
				},function(error) {
					deferred.reject(error);
				}, {timeout: 3000, enableHighAccuracy: true});

				return deferred.promise;	
			},

			getCurrentDatetime: function() {
				return moment().format('YYYY-MM-DD h:mm:ss');
			}
		};
	})
	.factory('SessionService', function() {
		var sessionData;

		return {
			setSessionData: function(currentSessionData) {
				sessionData = currentSessionData;
			},
			getSessionData: function() {
				return sessionData;
			}
		}
	})